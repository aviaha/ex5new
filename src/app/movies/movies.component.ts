import { Component, OnInit } from '@angular/core';

import {AngularFireDatabase,AngularFireList} from '@angular/fire/database';

@Component({
  selector: 'movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})



export class MoviesComponent implements OnInit {

  name= "no movie";

  movies = [ ];
  Studio = "";
  studios = [];
  Studio1="";


 displayedColumns: string[] = ['id', 'title', 'studio', 'weekendIncome','delete'];

 toDelete(element)
 {
   let x,y = this.movies;
   let z = element.id;

   if (this.movies.length == 1)
   {
     this.movies = [];
   }
   if (z > this.movies.length)
   {
     z = this.movies.length-1;
   }
   
   x = this.movies.slice(0,z-1);
   y = this.movies.slice(z, this.movies.length);
   this.movies = x;
   this.movies = this.movies.concat(y);
   this.name = element.title;
  
 }

  constructor(private db:AngularFireDatabase) { }
  
  filter() {
    let id = 1;
    this.db.list('/movies').snapshotChanges().subscribe(
      movies => {
        this.movies = [];
        movies.forEach(
          movie => {
            let y = movie.payload.toJSON();
            if (this.Studio == 'all') {
              this.movies.push(y);
            }
            else if (y['studio'] == this.Studio) {
              y['Id'] = id;
              id++;
              this.movies.push(y);
            }
          }
        )
      }
    )
  }

  ngOnInit() {

    this.db.list('/movies').snapshotChanges().subscribe(
      movies => {
        this.movies = [];
        this.studios = ['all'];
        movies.forEach(
          movie => {
            let y = movie.payload.toJSON();
            this.movies.push(y);
            let stu = y['studio'];
            if (this.studios.indexOf(stu) == -1) {
              this.studios.push(y['studio']);
            }
          }
        )
      }
    )
  }
  tofilter() {
    let id = 1;
    this.db.list('/movies').snapshotChanges().subscribe(
      movies => {
        this.movies = [];
        movies.forEach(
          movie => {
            let y = movie.payload.toJSON();
            if (this.Studio1 == 'all') {
              this.movies.push(y);
            }
            else if (y['studio'] == this.Studio1) {
              y['Id'] = id;
              id++;
              this.movies.push(y);
            }
          }
        )
      }
    )
  }
}